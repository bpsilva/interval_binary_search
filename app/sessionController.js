app.controller('sessionController', [ '$scope',

    function sessionController($scope) {
    var videoSize = 30;
    var images=[{
       image: 'A',
       time: 0     
    },{
       image: 'B',
       time: 3     
    },{
       image: 'C',
       time: 7     
    },{
       image: 'D',
       time: 12     
    },{
       image: 'E',
       time: 13     
    },{
       image: 'F',
       time: 15     
    },{
       image: 'G',
       time: 25     
    }];
    $scope.scopeImages = images;
    var onInterval = function(number, interval)
    {
        return number>=interval.begin && number<interval.end;
    }

    var intervals = [];
    for(var i = 1; i < images.length ; i++)
    {
        intervals.push({begin: images[i-1].time, end: images[i].time});
    }
    intervals.push({begin: images[images.length-1].time, end: videoSize+1});
    $scope.scopeInt = intervals;

    var intervalBinarySearch = function(number, firstIndex, lastIndex){
        console.log('binary search')
        if(lastIndex==firstIndex)
        {
            return firstIndex;
        }
        var centerIntervalIndex = firstIndex +Math.floor((lastIndex-firstIndex)/2);
        var centerInterval = intervals[centerIntervalIndex];
        if(onInterval(number, centerInterval))
        {
            return centerIntervalIndex;
        }
        if(centerInterval.begin> number)
        {
            return intervalBinarySearch(number, firstIndex, centerIntervalIndex-1);
        }
        if(centerInterval.end <= number)
        {
            return intervalBinarySearch(number, centerIntervalIndex+1, lastIndex);
        }
        console.log('default');
    }

    console.log(intervals);
    $scope.submit = function(number){
        $scope.result = images[intervalBinarySearch(number, 0, intervals.length)];
    }
}
]);